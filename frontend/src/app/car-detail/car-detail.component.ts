import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { CarService, Car } from "../car.service";

@Component({
  selector: "app-car-detail",
  templateUrl: "./car-detail.component.html",
  styleUrls: ["./car-detail.component.scss"],
})
export class CarDetailComponent implements OnInit {
  car: Car | null = null;

  constructor(
    private route: ActivatedRoute,
    private carService: CarService,
  ) {}

  ngOnInit(): void {
    this.fetchCar();
  }

  fetchCar(): void {
    const id = this.route.snapshot.paramMap.get("id");
    if (id) {
      this.carService.getCar(id).subscribe((car) => (this.car = car));
    }
  }
}
