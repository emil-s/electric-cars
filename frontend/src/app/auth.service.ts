import { Injectable } from "@angular/core";
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(private jwtHelper: JwtHelperService) {}

  login(token: string): void {
    localStorage.setItem("access_token", token);
  }

  logout(): void {
    localStorage.removeItem("access_token");
  }

  isAuthenticated(): boolean {
    const token = localStorage.getItem("access_token");
    return !this.jwtHelper.isTokenExpired(token || "");
  }
}
