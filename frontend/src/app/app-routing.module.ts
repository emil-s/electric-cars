import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CarListComponent } from "./car-list/car-list.component";
import { CarDetailComponent } from "./car-detail/car-detail.component";
import { LoginComponent } from "./login/login.component";
import { AuthGuardService } from "./auth.guard";

const routes: Routes = [
  {
    path: "",
    component: CarListComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: "car/:id",
    component: CarDetailComponent,
    canActivate: [AuthGuardService],
  },
  { path: "login", component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
