import { JwtModuleOptions } from "@auth0/angular-jwt";

export const jwtConfig: JwtModuleOptions = {
  config: {
    tokenGetter: () => {
      return localStorage.getItem("access_token");
    },
    allowedDomains: ["localhost"],
    disallowedRoutes: [],
  },
};
