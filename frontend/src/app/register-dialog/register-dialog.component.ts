import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material/dialog";

@Component({
  selector: "app-register-dialog",
  templateUrl: "./register-dialog.component.html",
  styleUrls: ["./register-dialog.component.scss"],
})
export class RegisterDialogComponent {
  constructor(public dialogRef: MatDialogRef<RegisterDialogComponent>) {}

  register(username: string, password: string): void {
    // In a real-world scenario, you'd send these credentials to your server for registration
    // Assume registration is successful
    this.dialogRef.close();
  }

  cancel(): void {
    this.dialogRef.close();
  }
}
