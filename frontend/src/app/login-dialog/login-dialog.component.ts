import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material/dialog";
import { AuthService } from "../auth.service";

@Component({
  selector: "app-login-dialog",
  templateUrl: "./login-dialog.component.html",
  styleUrls: ["./login-dialog.component.scss"],
})
export class LoginDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<LoginDialogComponent>,
    private authService: AuthService,
  ) {}

  login(username: string, password: string): void {
    // In a real-world scenario, you'd send these credentials to your server
    // Assume a successful login for now, with a token returned
    const mockToken = "mock-jwt-token";
    this.authService.login(mockToken);
    this.dialogRef.close();
  }

  cancel(): void {
    this.dialogRef.close();
  }
}
