import { Component } from "@angular/core";
import { AuthService } from "../auth.service";
import { MatDialog } from "@angular/material/dialog";
import { LoginDialogComponent } from "../login-dialog/login-dialog.component";
import { RegisterDialogComponent } from "../register-dialog/register-dialog.component";

@Component({
  selector: "app-nav-bar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"],
})
// ...
export class NavBarComponent {
  constructor(
    private authService: AuthService,
    private dialog: MatDialog,
  ) {}

  get isAuthenticated(): boolean {
    return this.authService.isAuthenticated();
  }

  login(): void {
    this.dialog.open(LoginDialogComponent);
  }

  register(): void {
    this.dialog.open(RegisterDialogComponent);
  }

  logout(): void {
    this.authService.logout();
  }
}
